pub mod parsers;
pub mod tokens;

pub use self::tokens::*;

#[derive(Debug)]
pub enum Error {
    StringExpected,
    UnexpectedEndOfString,
    Unknown,
}

#[derive(Copy, Clone)]
pub struct SourceLocation {
    byte_count: usize,
    line: usize,
    column: usize,
}

#[derive(Copy, Clone)]
pub struct ParseState<'a> {
    remaining: &'a str,
    source_location: SourceLocation,
}

pub fn parse_begin_object(state: ParseState) -> Result<(JsonBeginObject, ParseState), ()> {
    parsers::parse_begin_object(parsers::parse_opt_whitespaces(state))
}

pub fn parse_end_object(state: ParseState) -> Result<(JsonEndObject, ParseState), ()> {
    parsers::parse_end_object(parsers::parse_opt_whitespaces(state))
}

pub fn parse_begin_array(state: ParseState) -> Result<(JsonBeginArray, ParseState), ()> {
    parsers::parse_begin_array(parsers::parse_opt_whitespaces(state))
}

pub fn parse_end_array(state: ParseState) -> Result<(JsonEndArray, ParseState), ()> {
    parsers::parse_end_array(parsers::parse_opt_whitespaces(state))
}

pub fn parse_colon(state: ParseState) -> Result<(JsonColon, ParseState), ()> {
    parsers::parse_colon(parsers::parse_opt_whitespaces(state))
}

pub fn parse_comma(state: ParseState) -> Result<(JsonComma, ParseState), ()> {
    parsers::parse_comma(parsers::parse_opt_whitespaces(state))
}

pub fn parse_string<'a>(state: ParseState<'a>) -> Result<(JsonString<'a>, ParseState), Error> {
    parsers::parse_string(parsers::parse_opt_whitespaces(state))
}

pub fn parse_non_escaped_string<'a>(
    state: ParseState<'a>,
) -> Result<(JsonNonEscapedString<'a>, ParseState), Error> {
    parsers::parse_non_escaped_string(parsers::parse_opt_whitespaces(state))
}

pub fn parse_int(state: ParseState) -> Result<(JsonInt, ParseState), ()> {
    parsers::parse_int(parsers::parse_opt_whitespaces(state))
}

pub fn parse_float(state: ParseState) -> Result<(JsonFloat, ParseState), ()> {
    parsers::parse_float(parsers::parse_opt_whitespaces(state))
}

pub fn parse_null(state: ParseState) -> Result<(JsonNull, ParseState), ()> {
    parsers::parse_null(parsers::parse_opt_whitespaces(state))
}

pub fn parse_true(state: ParseState) -> Result<(JsonTrue, ParseState), ()> {
    parsers::parse_true(parsers::parse_opt_whitespaces(state))
}

pub fn parse_false(state: ParseState) -> Result<(JsonFalse, ParseState), ()> {
    parsers::parse_false(parsers::parse_opt_whitespaces(state))
}

pub fn parse_bool(state: ParseState) -> Result<(JsonBool, ParseState), ()> {
    parsers::parse_bool(parsers::parse_opt_whitespaces(state))
}

pub fn parse_string_literal<'a>(
    state: ParseState<'a>,
    literal: &str,
) -> Result<((), ParseState<'a>), ()> {
    let (parsed_str, st) = parse_string(state).map_err(|_| ())?;
    if parsed_str.matches(literal) {
        Ok(((), st))
    } else {
        Err(())
    }
}

pub fn parse_eof(state: ParseState) -> Result<(JsonEof, ParseState), ()> {
    let state = parsers::parse_opt_whitespaces(state);
    if state.remaining.is_empty() {
        Ok((JsonEof, state))
    } else {
        Err(())
    }
}

impl From<()> for Error {
    fn from(f: ()) -> Self {
        Error::Unknown
    }
}

#[test]
fn it_parses_complex_object() -> Result<(), Error> {
    let st = ParseState {
        remaining: r#"{
        "type": "FeatureCollection",
        "geometry": [
            -65.613616999999977,43.420273000000009
         ]}

"#,
        source_location: SourceLocation {
            byte_count: 0,
            line: 0,
            column: 0,
        },
    };

    let (_, st) = parse_begin_object(st)?;
    let (_, st) = parse_string_literal(st, "type")?;
    let (_, st) = parse_colon(st)?;
    let (_, st) = parse_string_literal(st, "FeatureCollection")?;
    let (_, st) = parse_comma(st)?;
    let (_, st) = parse_string_literal(st, "geometry")?;
    let (_, st) = parse_colon(st)?;
    let (_, st) = parse_begin_array(st)?;
    let (_, st) = parse_float(st)?;
    let (_, st) = parse_comma(st)?;
    let (_, st) = parse_float(st)?;
    let (_, st) = parse_end_array(st)?;
    let (_, st) = parse_end_object(st)?;
    let (_, st) = parse_eof(st)?;

    assert_eq!(st.remaining, "");

    Ok(())
}
