use crate::{tokens::*, Error, ParseState, SourceLocation};

pub fn parse_opt_whitespaces(state: ParseState) -> ParseState {
    let mut chars = state.remaining.chars();
    let mut bytes_count = 0;

    loop {
        match chars.next() {
            Some(ch) if char::is_whitespace(ch) => {
                bytes_count += ch.len_utf8();
            }
            _ => break,
        }
    }

    ParseState {
        remaining: &state.remaining[bytes_count..],
        source_location: state.source_location,
    }
}

fn parse_char(state: ParseState, expected_ch: char) -> Result<ParseState, ()> {
    let mut chars = state.remaining.chars();
    match chars.next() {
        Some(ch) if ch == expected_ch => Ok(ParseState {
            remaining: chars.as_str(),
            source_location: state.source_location,
        }),
        _ => Err(()),
    }
}

pub fn parse_begin_object(state: ParseState) -> Result<(JsonBeginObject, ParseState), ()> {
    parse_char(state, '{').map(|next_state| (JsonBeginObject, next_state))
}

pub fn parse_end_object(state: ParseState) -> Result<(JsonEndObject, ParseState), ()> {
    parse_char(state, '}').map(|next_state| (JsonEndObject, next_state))
}

pub fn parse_begin_array(state: ParseState) -> Result<(JsonBeginArray, ParseState), ()> {
    parse_char(state, '[').map(|next_state| (JsonBeginArray, next_state))
}

pub fn parse_end_array(state: ParseState) -> Result<(JsonEndArray, ParseState), ()> {
    parse_char(state, ']').map(|next_state| (JsonEndArray, next_state))
}

pub fn parse_colon(state: ParseState) -> Result<(JsonColon, ParseState), ()> {
    parse_char(state, ':').map(|next_state| (JsonColon, next_state))
}

pub fn parse_comma(state: ParseState) -> Result<(JsonComma, ParseState), ()> {
    parse_char(state, ',').map(|next_state| (JsonComma, next_state))
}

pub fn parse_string<'a>(state: ParseState<'a>) -> Result<(JsonString<'a>, ParseState), Error> {
    // XXX
    parse_non_escaped_string(state).map(|(JsonNonEscapedString(value), next_state)| {
        (JsonString::NonEscaped(value), next_state)
    })
}

pub fn parse_non_escaped_string<'a>(
    state: ParseState<'a>,
) -> Result<(JsonNonEscapedString<'a>, ParseState), Error> {
    let state = parse_char(state, '"').map_err(|()| Error::StringExpected)?;

    let mut chars = state.remaining.chars();
    let mut bytes_count = 0;

    loop {
        match chars.next() {
            Some('"') => {
                break;
            }
            Some(ch) => {
                bytes_count += ch.len_utf8();
            }
            None => {
                return Err(Error::UnexpectedEndOfString);
            }
        }
    }

    Ok((
        JsonNonEscapedString(&state.remaining[..bytes_count]),
        ParseState {
            remaining: chars.as_str(),
            ..state
        },
    ))
}

pub fn parse_int(state: ParseState) -> Result<(JsonInt, ParseState), ()> {
    let mut chars = state.remaining.chars();

    let mut bytes_count = 0;

    if let Some(ch) = chars.next() {
        match ch {
            '+' | '-' => bytes_count += ch.len_utf8(),
            ch if char::is_digit(ch, 10) => bytes_count += ch.len_utf8(),
            _ => return Err(()),
        }
    } else {
        return Err(());
    }

    bytes_count += read_digits(&mut chars).0;

    let (int_str, remaining) = &state.remaining.split_at(bytes_count);
    let int = isize::from_str_radix(int_str, 10).map_err(|_| ())?;

    Ok((JsonInt(int), ParseState { remaining, ..state }))
}

pub fn parse_float(state: ParseState) -> Result<(JsonFloat, ParseState), ()> {
    let mut chars = state.remaining.chars();

    let mut bytes_count = 0;

    if let Some(ch) = chars.next() {
        match ch {
            '+' | '-' => bytes_count += ch.len_utf8(),
            ch if char::is_digit(ch, 10) => bytes_count += ch.len_utf8(),
            _ => return Err(()),
        }
    } else {
        return Err(());
    }

    match read_digits(&mut chars) {
        (cnt, Some('.')) => {
            bytes_count += cnt;
            bytes_count += '.'.len_utf8();
        }
        _ => {
            return Err(());
        }
    }

    let (cnt, _next_char) = read_digits(&mut chars);

    bytes_count += cnt;

    let (float_str, remaining) = &state.remaining.split_at(bytes_count);
    let float = float_str.parse::<f64>().map_err(|_| ())?;

    Ok((JsonFloat(float), ParseState { remaining, ..state }))
}

fn read_digits(chars: &mut std::str::Chars) -> (usize, Option<char>) {
    let mut bytes_count = 0;
    while let Some(ch) = chars.next() {
        if char::is_digit(ch, 10) {
            bytes_count += ch.len_utf8()
        } else {
            return (bytes_count, Some(ch));
        }
    }
    (bytes_count, None)
}

fn parse_literal<'a>(state: ParseState<'a>, expected_literal: &str) -> Result<ParseState<'a>, ()> {
    let mut chars = state.remaining.chars();
    let () = expect_next_str(&mut chars, expected_literal).map_err(|_| ())?;
    let () = expect_peek_next_delim_or_eof(chars.clone())?;

    Ok(ParseState {
        remaining: chars.as_str(),
        ..state
    })
}

pub fn parse_null(state: ParseState) -> Result<(JsonNull, ParseState), ()> {
    parse_literal(state, "null").map(|next_state| (JsonNull, next_state))
}

pub fn parse_true(state: ParseState) -> Result<(JsonTrue, ParseState), ()> {
    parse_literal(state, "true").map(|next_state| (JsonTrue, next_state))
}

pub fn parse_false(state: ParseState) -> Result<(JsonFalse, ParseState), ()> {
    parse_literal(state, "false").map(|next_state| (JsonFalse, next_state))
}

fn parse_true_as_bool(state: ParseState) -> Result<(JsonBool, ParseState), ()> {
    parse_true(state).map(|(_, next_state)| (JsonBool(true), next_state))
}

fn parse_false_as_bool(state: ParseState) -> Result<(JsonBool, ParseState), ()> {
    parse_false(state).map(|(_, next_state)| (JsonBool(false), next_state))
}

pub fn parse_bool(state: ParseState) -> Result<(JsonBool, ParseState), ()> {
    parse_true_as_bool(state).or_else(|_| parse_false_as_bool(state))
}

fn expect_next_char(
    chars: &mut std::str::Chars,
    expected_char: char,
) -> Result<(), (Option<char>, char)> {
    match chars.next() {
        Some(ch) if ch == expected_char => Ok(()),
        ch @ _ => Err((ch, expected_char)),
    }
}

fn expect_next_str(
    chars: &mut std::str::Chars,
    expected_str: &str,
) -> Result<(), (Option<char>, char)> {
    for ch in expected_str.chars() {
        let () = expect_next_char(chars, ch)?;
    }
    Ok(())
}

fn expect_peek_next_delim_or_eof(mut chars: std::str::Chars) -> Result<(), ()> {
    match chars.next() {
        Some(',' | '}' | ']' | ':') => Ok(()),
        Some(ch) if char::is_whitespace(ch) => Ok(()),
        None => Ok(()),
        _ => Err(()),
    }
}

#[test]
fn it_parses_null() {
    let st = ParseState {
        remaining: "null,",
        source_location: SourceLocation {
            byte_count: 0,
            line: 0,
            column: 0,
        },
    };

    let parsed_null = parse_null(st).unwrap();
    assert_eq!(parsed_null.1.remaining, ",");
}

#[test]
fn it_parses_a_bool() {
    let st = ParseState {
        remaining: "true,",
        source_location: SourceLocation {
            byte_count: 0,
            line: 0,
            column: 0,
        },
    };

    let parsed_bool = parse_bool(st).unwrap();
    assert_eq!(parsed_bool.0, JsonBool(true));
    assert_eq!(parsed_bool.1.remaining, ",");
}

#[test]
fn it_fails_to_parse_null_when_not_followed_by_delim_or_eof() {
    let st = ParseState {
        remaining: "null_",
        source_location: SourceLocation {
            byte_count: 0,
            line: 0,
            column: 0,
        },
    };

    assert!(parse_null(st).is_err());
}

#[test]
fn it_parses_unescaped_string() {
    let st = ParseState {
        remaining: r#""a string""#,
        source_location: SourceLocation {
            byte_count: 0,
            line: 0,
            column: 0,
        },
    };

    let parsed_string = parse_non_escaped_string(st).unwrap();
    assert_eq!(parsed_string.0, JsonNonEscapedString("a string"));
    assert_eq!(parsed_string.1.remaining, "");
}

#[test]
fn it_parses_an_int() {
    let st = ParseState {
        remaining: r#"-123456,"#,
        source_location: SourceLocation {
            byte_count: 0,
            line: 0,
            column: 0,
        },
    };

    let parsed_int = parse_int(st).unwrap();
    assert_eq!(parsed_int.0, JsonInt(-123456));
    assert_eq!(parsed_int.1.remaining, ",");
}

#[test]
fn it_parses_a_float() {
    let st = ParseState {
        remaining: r#"123.44444555"#,
        source_location: SourceLocation {
            byte_count: 0,
            line: 0,
            column: 0,
        },
    };

    let parsed_int = parse_float(st).unwrap();
    assert_eq!(parsed_int.0, JsonFloat(123.44444555));
    assert_eq!(parsed_int.1.remaining, "");
}
