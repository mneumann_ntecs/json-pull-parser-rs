#[derive(Copy, Clone)]
pub struct JsonEof;

#[derive(Copy, Clone)]
pub struct JsonBeginObject;

#[derive(Copy, Clone)]
pub struct JsonEndObject;

#[derive(Copy, Clone)]
pub struct JsonBeginArray;

#[derive(Copy, Clone)]
pub struct JsonEndArray;

#[derive(Copy, Clone)]
pub struct JsonNull;

#[derive(Copy, Clone)]
pub struct JsonTrue;

#[derive(Copy, Clone)]
pub struct JsonFalse;

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct JsonBool(pub bool);

#[derive(Copy, Clone)]
pub struct JsonColon;

#[derive(Copy, Clone)]
pub struct JsonComma;

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct JsonNonEscapedString<'a>(pub &'a str);

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum JsonString<'a> {
    NonEscaped(&'a str),
    Escaped(&'a str),
}

impl<'a> JsonString<'a> {
    pub fn matches(&self, s: &str) -> bool {
        match self {
            &JsonString::NonEscaped(s2) => s2.eq(s),
            _ => unimplemented!(),
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct JsonInt(pub isize);

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct JsonFloat(pub f64);
